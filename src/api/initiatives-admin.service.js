import jquery from 'jquery'
import { getCookie } from './helpers'

let HEADERS = {}
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`
}

function adminGetInitiatives (limit = 50, skip = 0) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/agora2030?limit=${limit}&skip=${skip}&scope=admin`,
      type: 'GET',
      crossDomain: true,
      dataType: 'json',
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        reject(xhr)
        if (xhr.status === 403 || xhr.status === 401) {
          window.location.href = '/403'
        }
      }
    })
  })
}

function adminPublishInitiative (initiativeId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/publish`,
      type: 'POST',
      crossDomain: true,
      dataType: 'json',
      headers: HEADERS,
      data: { status: true },
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        reject(xhr)
        if (xhr.status === 403 || xhr.status === 401) {
          window.location.href = '/403'
        }
      }
    })
  })
}

function adminUnpublishInitiative (initiativeId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/publish`,
      type: 'POST',
      crossDomain: true,
      dataType: 'json',
      headers: HEADERS,
      data: { status: false },
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        reject(xhr)
        if (xhr.status === 403 || xhr.status === 401) {
          window.location.href = '/403'
        }
      }
    })
  })
}

function adminGetInitiativesComments (initiativeId, limit = 50, skip = 0) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/comments?limit=${limit}&skip=${skip}`,
      type: 'GET',
      crossDomain: true,
      dataType: 'json',
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        reject(xhr)
        if (xhr.status === 403 || xhr.status === 401) {
          window.location.href = '/403'
        }
      }
    })
  })
}

function adminDeleteInitiativesComments (initiativeId, commentId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}initiatives/${initiativeId}/comments/${commentId}`,
      type: 'DELETE',
      crossDomain: true,
      dataType: 'text',
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        reject(xhr)
        if (xhr.status === 403 || xhr.status === 401) {
          window.location.href = '/403'
        }
      }
    })
  })
}


export {
  adminGetInitiatives,
  adminPublishInitiative,
  adminUnpublishInitiative,
  adminGetInitiativesComments,
  adminDeleteInitiativesComments
}
