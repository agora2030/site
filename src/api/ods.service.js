import axios from 'axios'
import { getCookie } from './helpers'

const EP = {
  get: `${process.env.SERVER}ods?limit=17`,
  add: (id) => `${process.env.SERVER}initiatives/${id}/ods`,
  delete: (id, odsId) => `${process.env.SERVER}initiatives/${id}/ods/${odsId}`
}

let HEADERS = {}
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`
}

const getOds = () => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: EP.get,
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)

      reject(error)
    })
  })
}

const addOds = (id, odsId) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: EP.add(id),
      headers: HEADERS,
      data: { odsId },
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)

      reject(error)
    })
  })
}

const deleteOds = (id, odsId) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'delete',
      url: EP.delete(id, odsId),
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)

      reject(error)
    })
  })
}

export {
  getOds,
  addOds,
  deleteOds
}
