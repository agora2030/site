import axios from 'axios'
import { getCookie, createCookie } from './helpers'

const EP = {
  register: `${process.env.SERVER}signup`,
  login: `${process.env.SERVER}login`,
  profile: `${process.env.SERVER}profile?scope=resume`
}

let HEADERS = {}
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`
}

const registerUser = ({ firstName, lastName, email, password, country }) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: EP.register,
      data: { firstName, lastName, email, password, country }
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

const loginUser = ({ email, password }) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: EP.login,
      headers: HEADERS,
      data: { email, password }
    })
    .then(success => {
      resolve(success)
      createCookie('agoraToken', success.data.token, 7)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

const getUserProfile = (token) => {
  return new Promise((resolve, reject) => {
    let h = {}
    if (token) {
      h.Authorization = `Bearer ${token}`
    } else {
      h = { ...HEADERS }
    }
    axios({
      method: 'get',
      url: EP.profile,
      headers: h
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

const confirmEmail = (token) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `${process.env.SERVER}confirmation/${token}`
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

const changePassword = (emailToken, password) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${process.env.SERVER}recovery/password/${emailToken}`,
      data: { password }
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

export {
  registerUser,
  loginUser,
  getUserProfile,
  confirmEmail,
  changePassword
}
