import axios from 'axios'
import { getCookie } from './helpers'

const EP = {
  create: `${process.env.SERVER}initiatives`,
  update: (id) => `${process.env.SERVER}initiatives/${id}`,
  get: (id) => `${process.env.SERVER}initiatives/agora2030/${id}?scope=admin`,
  land: {
    get: (slug) => `${process.env.SERVER}initiatives/${slug}`,
    getAll: (limit, skip, filter) => {
      let f = ''
      if (filter && (filter.q || filter.ods || filter.actionType)) {
        if (!filter.q) {
          delete filter.q
        }
        if (!filter.ods || filter.ods === '0') {
          delete filter.ods
        }
        if (!filter.actionType) {
          delete filter.actionType
        }
        f = `&filter=${JSON.stringify(filter)}`
      }
      return `${process.env.SERVER}initiatives?limit=${limit}&skip=${skip}${f}`
    },
    getAnswers: (id) => `${process.env.SERVER}initiatives/${id}/answers`
  }
}

let HEADERS = {}
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`
}

const createInitiative = (data) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: EP.create,
      data: data,
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

const updateInitiative = (data, id) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'patch',
      url: EP.update(id),
      data: data,
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

const getInitiative = (id) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `${process.env.SERVER}initiatives/agora2030/${id}`,
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

const getLandInitiative = (slug) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: EP.land.get(slug),
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

const getLandInitiatives = (limit = 9, skip = 0, filter) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: EP.land.getAll(limit, skip, filter),
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

const getInitiativeAnswers = (id) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: EP.land.getAnswers(id),
      headers: HEADERS,
      crossDomain: true
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

export {
  createInitiative,
  getInitiative,
  updateInitiative,
  getLandInitiative,
  getLandInitiatives,
  getInitiativeAnswers
}
