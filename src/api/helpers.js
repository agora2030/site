function bytesToSize(bytes) {
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
  if (bytes === 0) return 'n/a'
  const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
  if (i === 0) return `${bytes}${sizes[i]})`
  return `${(bytes / (1024 ** i)).toFixed(1)}${sizes[i]}`
}


function sizeToBytes(size) {
  const sizes = [
    {
      unity: 'KB',
      multiplicador: 1000
    },
    {
      unity: 'MB',
      multiplicador: 1000000
    },
    {
      unity: 'GB',
      multiplicador: 1000000000
    }
  ]
  for (var i = 0; i < sizes.length; i++) {
    if (size.toLocaleUpperCase().indexOf(sizes[i].unity) > -1) {
      return Number(size.slice(0, -2))*sizes[i].multiplicador;
    }
  }
}

function validateUrl(value) {
  return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value);
}

function checkUrlSource(url, sources) {
  if (validateUrl(url)) {
    let itsFromSource = false;
    sources.map(source => {
      if (url.indexOf(source) > -1) {
        itsFromSource = true;
      }
    })
    return itsFromSource;
  }
  return false;
}

function getCookie(tokenname) {
  let name = tokenname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
          c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
      }
  }
}

function createCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function prepareFormData(obj) {
  let o = Object.assign({}, obj);
  for (let key in o) {
    if (o[key] == null || typeof o[key] == undefined || o[key].length == 0) {
      delete o[key];
    }
  }
  return o;
}

function openPopup(w, h, url) {
  let size = `width=${w},height=${h},resizable,scrollbars=yes,status=1`;
  window.open(url,'sharer',size);
}



export {
  bytesToSize,
  validateUrl,
  checkUrlSource,
  getCookie,
  prepareFormData,
  sizeToBytes,
  openPopup,
  createCookie
}
