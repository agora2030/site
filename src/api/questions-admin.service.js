import jquery from 'jquery'
import { getCookie } from './helpers'

let HEADERS = {}
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`
}

function adminGetQuestions (limit = 40, skip = 0) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}questions?limit=${limit}&skip=${skip}`,
      type: 'GET',
      crossDomain: true,
      dataType: 'json',
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        reject(xhr)
        if (xhr.status === 403 || xhr.status === 401) {
          window.location.href = '/403'
        }
      }
    })
  })
}

const addQuestion = (data) => {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}questions`,
      type: 'POST',
      crossDomain: true,
      dataType: 'json',
      data,
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        reject(xhr)
        if (xhr.status === 403 || xhr.status === 401) {
          window.location.href = '/403'
        }
      }
    })
  })
}

function adminGetQuestion (questionId) {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}questions/${questionId}`,
      type: 'GET',
      crossDomain: true,
      dataType: 'json',
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        reject(xhr)
        if (xhr.status === 403 || xhr.status === 401) {
          window.location.href = '/403'
        }
      }
    })
  })
}

const adminUpdateQuestion = (questionId, data) => {
  return new Promise((resolve, reject) => {
    jquery.ajax({
      url: `${process.env.SERVER}questions/${questionId}`,
      type: 'PATCH',
      crossDomain: true,
      dataType: 'json',
      data,
      headers: HEADERS,
      success: (response) => {
        resolve(response)
      },
      error: (xhr, status) => {
        reject(xhr)
        if (xhr.status === 403 || xhr.status === 401) {
          window.location.href = '/403'
        }
      }
    })
  })
}

export {
  adminGetQuestions,
  addQuestion,
  adminGetQuestion,
  adminUpdateQuestion
}
