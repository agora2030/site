# Site e Admin

> Projeto Vue.js para exposição das soluções do desafio ágora e ambiente admistrativo do desafio

## Requisitos e instalação

 - nodejs: ">= 4.0.0"
 - npm: ">= 3.0.0"

``` bash

# Dentro da raiz do projeto instale as dependências:
npm install

# Para executar em ambiente de desenvolvimento (localhost:8080):
npm run dev

# Gerar build para production
npm run build

```

### Na pasta ```/config/env``` estão os arquivos referêntes a cada ambiente que devem ser configurados de acordo com a necessidade.

