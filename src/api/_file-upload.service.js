import axios from 'axios';

import { getCookie } from './helpers'

const EP = {
  upload: (id) =>  `${process.env.SERVER}initiatives/${id}/medias`,
  delete: (id, mediaId) => `${process.env.SERVER}initiatives/${id}/medias/${mediaId}`,
  get: (id, type) => `${process.env.SERVER}initiatives/${id}/medias?type=${type}`
};

let HEADERS = {};
if (getCookie('agoraToken')) {
  HEADERS.Authorization = `Bearer ${getCookie('agoraToken')}`;
}

const upload = ({ formData, initiativeId }) => {
  return new Promise((resolve, reject) => {

    axios({
      method: 'post',
      url: EP.upload(initiativeId),
      data: formData,
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success);
    })
    .catch(error => {
      console.log(error);
      reject(error);
    });

  })
}

const deleteMedia = ({ initiativeId, mediaId }) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'delete',
      url: EP.delete(initiativeId, mediaId),
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success);
    })
    .catch(error => {
      console.log(error);
      reject(error);
    });
  })
}

const getMedias = (type, initiativeId) => {
  return new Promise((resolve, reject) => {

    axios({
      method: 'get',
      url: EP.get(initiativeId, type),
      headers: HEADERS,
      crossDomain: true
    })
    .then(success => {
      resolve(success);
    })
    .catch(error => {
      console.log(error);
      reject(error);
    });

  })
}

const batchUpload = (arrayData, initiativeId) => {
  let arrayRequests = [];

  arrayData.forEach(item => {
    
    const request = () => {
      return axios({
        method: 'post',
        url: EP.upload(initiativeId),
        data: item,
        headers: HEADERS,
        crossDomain: true
      })
      .then(success => {
        return success;
      })
      .catch(error => {
        console.log(error);
        return error;
      });
    }

    arrayRequests.push(request);
  })

  return new Promise((resolve, reject) => {

    axios.all(arrayRequests)
      .then(axios.spread(function (acct, perms) {
        console.log("upload finalizado");
        console.log(acct, perms);
        resolve();
      }));

    // jquery.when.apply(null, arrayRequests).then(function() {
    //   let resp = [];
    //   if (arrayRequests.length == 1) {
    //     resp.push(arguments[0]);
    //   } else {
    //     for (let i = 0; i < arguments.length; i++) {
    //       resp.push(arguments[i][0]);
    //     }
    //   }
    //   resolve(resp);
    // }).done(() => {
    //   console.log("upload finalizado");
    // })

  })
}

export {
  upload,
  deleteMedia,
  getMedias,
  batchUpload
}
