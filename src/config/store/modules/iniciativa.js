import { createInitiative, updateInitiative } from '../../../api/initiative.service'

const state = {
  current: {},
  list: []
}

const getters = {
  getCurrentIniciativa: (state) => state.current,
  getAllIniciativas: (state) => state.all
}

const mutations = {
  saveCurrent (state, payload) {
    state.current = payload
  },
  saveAll (state, payload) {
    state.list = payload
  }
}

const actions = {
  saveCurrentIniciativa ({state, commit}, payload) {
    return new Promise((resolve, reject) => {
      if (payload.onlyStore === true) {
        commit('saveCurrent', payload.data)
        resolve()
      } else {
        createInitiative(payload.data)
          .then(
            success => {
              console.log(success)
              commit('saveCurrent', success.data)
              resolve(success)
            },
            error => {
              reject(error)
            }
          )
      }
    })
  },
  updateCurrentIniciativa ({state, commit}, payload) {
    return new Promise((resolve, reject) => {
      if (payload.onlyStore === true) {
        commit('saveCurrent', payload.data)
        resolve()
      } else {
        updateInitiative(payload.data, payload.id)
          .then(
            success => {
              console.log(success)
              commit('saveCurrent', success.data)
              resolve(success)
            },
            error => {
              reject(error)
            }
          )
      }
    })
  },
  saveListIniciativa ({state, commit}, payload) {
    commit('saveAll', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
